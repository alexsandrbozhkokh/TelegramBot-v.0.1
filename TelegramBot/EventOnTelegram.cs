﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InputFiles;
using File = Telegram.Bot.Types.File;

namespace TelegramBot
{
    internal static class EventOnTelegram
    { 
       private static  long  _chatId = 0;

       private static Update _update;

       private static ITelegramBotClient _bot;

       private static String _pathFolder;
        // Метод получает данные про файл, если  это не PDF или размер больше 20мб. сообщает пользователю об ошибке. Если все хорошо скачивает файл из чата пользователя. 
        public static async Task GetFileAsync(Update update, ITelegramBotClient botClient)
        {
            _bot = botClient;
            _update = update;
            _chatId = update.Message.Chat.Id;
            var documents = _update.Message.Document;//Данные про файл.
            string extension = Path.GetExtension(documents.FileName); // Получение формат файла.
         
            if (extension != ".pdf")
            {
                Message message =
                    await _bot.SendTextMessageAsync(_chatId, "Это не PDF пошел нахуй");
                return;
            }
            if (documents.FileSize > 20971520)
            {
                Message message =
                    await _bot.SendTextMessageAsync(_chatId, "Фаил больше 20мб ");
                return;
            }



            var FileFromChat = Task<File>.Run(() => _bot.GetFileAsync(documents.FileId).Result).Result; //Получение файла из чата.
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId + "GetFileAsync");
            // var messag1e = await botClient.SendDocumentAsync(chatId, new InputOnlineFile(documents.FileId));
            ;
            await Task.Run(() =>DownloadFile(FileFromChat, documents));

            SendDocument();
        }

        // Метод скачивает PDF файл из чата и сохраняет его в системе 
        private static string DownloadFile(File path, Document documents)
        {
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId + "DownloadFile");
            string LinkOnFile = @"https://api.telegram.org/file/bot5357791234:AAEPstS2yJ8AvpraITcqtU5Ge4Gxms1CCKw/" + $"{path.FilePath}" ; // Cсылка на файл.
      
              _pathFolder = ("F:\\TelegaBOtConvetPDFtoMP3\\" + $"{_chatId.ToString()}");// куда сохранять.
             if (Directory.Exists(_pathFolder) == false) // Проверка есть-ли такая папка с chatId. Если нет то создает
            {
                 DirectoryInfo drInfo = new DirectoryInfo(_pathFolder);
                 drInfo.Create();
             } 
             using (var client = new System.Net.WebClient())
             {
                 client.DownloadFileAsync(new Uri(LinkOnFile), _pathFolder + "\\" + $"{documents.FileName}");// Скачивание и сохранение файл.

             }
            ConvectorPDF.Convector(_pathFolder, documents.FileName);
           
            return _pathFolder + "\\" + "YourAudioBook.wav"; // Возвращает полный путь к файлу PDF


        }

        private static async Task SendDocument()
        {
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId + "SendDocument");
            Thread.Sleep(2000);
            using (var stream = System.IO.File.Open(_pathFolder + "\\" + "YourAudioBook.wav", FileMode.Open) )
            {
                InputOnlineFile iof = new InputOnlineFile(stream);
                iof.FileName = "YourAudioBook.wav";
                var send =  await _bot.SendDocumentAsync(_chatId, iof, "Сообщение");
            }
        }

    }
}
