﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;

namespace TelegramBot
{
    internal class  TelegramBot
    {
        private readonly string tokenAPITelegramBot = "5357791234:AAEPstS2yJ8AvpraITcqtU5Ge4Gxms1CCKw";
        public async Task TelegramBotStart()
        {
            using var cts = new CancellationTokenSource(); 
            TelegramBotClient botClient = new TelegramBotClient(tokenAPITelegramBot);
            
            var receiverOptions = new ReceiverOptions
            {
                AllowedUpdates = {} // receive all update types
            };
            botClient.StartReceiving(
                HandleUpdateAsync,
                HandleErrorAsync,
                receiverOptions,
                cancellationToken: cts.Token);
       
            var me = await botClient.GetMeAsync();
            Console.WriteLine($"Start listening for @{me.Username}");


        }



        async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {

            #region MyRegion

            try
            {
                Message message =
                    await botClient.SendTextMessageAsync(update!.Message.Chat.Id,
                        "Bot is under development" + " Бот находится в разработке ");
                if (update.Message!.Type == MessageType.Document)

                    await EventOnTelegram.GetFileAsync(update, botClient);

                // Only process Message updates: https://core.telegram.org/bots/api#message
                if (update.Type != UpdateType.Message)
                    return;
                // Only process text messages
                if (update.Message!.Type != MessageType.Document)
                    return;

            }
            catch
            {
                Console.WriteLine("Пидар обнаружен");
            }

           





            #endregion


        }
        Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            var ErrorMessage = exception switch
            {
                ApiRequestException apiRequestException => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}", _ => exception.ToString()
            };

            Console.WriteLine(ErrorMessage);
            return Task.CompletedTask;
        }

    }
}
