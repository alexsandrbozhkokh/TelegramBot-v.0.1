﻿using BitMiracle.Docotic.Pdf;
using Microsoft.CognitiveServices.Speech;
using System;
using System.Collections.Generic;
using System.Threading;

namespace TelegramBot
{
    internal static class ConvectorPDF
    {
        const string YourSubscriptionKey = "cc755f6f35e34cb6930e6497db3b3117";
        const string YourServiceRegion = "westeurope";
        private static string _text { get; set; }
        private static List<string> _chaptersList = new();
        public static void Convector(string path, string DocumentName)
        {
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId + "Convector");
            Thread.Sleep(2000);
            using (var pdf = new PdfDocument(path + "\\" + DocumentName))
            {

                _text = pdf.GetText().ToLower();
                TextСleaning();
            }


            SpeechConfigAsync(_text, path, DocumentName);
            DivisionByChapter(_text.LastIndexOf("глава"));
        }


        static void SpeechConfigAsync(String Text, string path, string documentName)
        {

            Console.WriteLine(Thread.CurrentThread.ManagedThreadId + "SpeechConfigAsync");

            var speechConfig = SpeechConfig.FromSubscription(YourSubscriptionKey, YourServiceRegion);
            // The language of the voice that speaks.
            speechConfig.SpeechSynthesisVoiceName = "ru-RU-SvetlanaNeural";

            var Document = documentName.Remove(documentName.Length - 4, 4);
            using (var speechSynthesizer = new SpeechSynthesizer(speechConfig, null))
            {


                //int sd = 0;

                //StringBuilder sb = new StringBuilder();
                //Console.WriteLine(DeletingСharacters.Length);
                //if (DeletingСharacters.Length > 65536 / 2 - 20)
                //{
                //    sd = 65536 / 2 - 20;

                //}
                //else
                //{
                //    sd = DeletingСharacters.Length;
                //}
                //for (int i = 0; i < sd; i++)
                //{
                //    sb.Append(DeletingСharacters[i]);
                //}
                // var speechSynthesisResult =  speechSynthesizer.SpeakTextAsync(sb.ToString());
                // Console.WriteLine(speechSynthesisResult.Result.ResultId);
                // Console.WriteLine(SpeechSynthesisCancellationDetails.FromResult(speechSynthesisResult.Result));
                // OutputSpeechSynthesisResult(speechSynthesisResult.Result, sb.ToString());
                // using var stream = AudioDataStream.FromResult(speechSynthesisResult.Result);
                // stream.SaveToWaveFileAsync($"{path}\\YourAudioBook.wav");


            }


        }
        static void OutputSpeechSynthesisResult(SpeechSynthesisResult speechSynthesisResult, string text)
        {
            switch (speechSynthesisResult.Reason)
            {
                case ResultReason.SynthesizingAudioCompleted:
                    Console.WriteLine($"Speech synthesized for text: [{text}]");
                    break;
                case ResultReason.Canceled:
                    var cancellation = SpeechSynthesisCancellationDetails.FromResult(speechSynthesisResult);
                    Console.WriteLine($"CANCELED: Reason={cancellation.Reason}");

                    if (cancellation.Reason == CancellationReason.Error)
                    {
                        Console.WriteLine($"CANCELED: ErrorCode={cancellation.ErrorCode}");
                        Console.WriteLine($"CANCELED: ErrorDetails=[{cancellation.ErrorDetails}]");
                        Console.WriteLine($"CANCELED: Did you set the speech resource key and region values?");
                    }
                    break;
                default:
                    break;
            }

        }

        private static void TextСleaning()
        {
            _text = _text.Replace(Environment.NewLine, "")
                .Replace(",", "")
                .Replace(".", "")
                .Replace("!", "")
                .Replace("-", "")
                .Replace("–", "");
        }

        private static void DivisionByChapter(int indexLast)
        {

            string pars = "глава";
            string temp = string.Empty;
            bool b = _text.Contains(pars);

            for (int i = indexLast; i < _text.Length; i++)
            {
                temp += _text[i];
            }
            _chaptersList.Add(temp);
            _text = _text.Remove(indexLast, _text.Length - indexLast);
            if (_text.Contains(pars))
            {
                DivisionByChapter(_text.LastIndexOf(pars));
            }
            else
            {
                _chaptersList.Add(_text);
            }


        }
    }
}
