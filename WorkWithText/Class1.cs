﻿using System;

namespace WorkWithText
{
    public class Class1
    {
        private static void TextСleaning()
        {
            _text = _text.Replace(Environment.NewLine, "")
                .Replace(",", "")
                .Replace(".", "")
                .Replace("!", "")
                .Replace("-", "")
                .Replace("–", "");
        }

        private static void DivisionByChapter(int indexLast)
        {

            string pars = "глава";
            string temp = string.Empty;
            bool b = _text.Contains(pars);

            for (int i = indexLast; i < _text.Length; i++)
            {
                temp += _text[i];
            }
            _chaptersList.Add(temp);
            _text = _text.Remove(indexLast, _text.Length - indexLast);
            if (_text.Contains(pars))
            {
                DivisionByChapter(_text.LastIndexOf(pars));
            }
            else
            {
                _chaptersList.Add(_text);
            }


        }
    }
}
